
$(document).ready(function(){

    $("#button").click(function(){
    var month = $("#MonthInput").val();
    var day = $("#DayInput").val();
    var year = $("#YearInput").val();

    var age = calcAge(year);
    
    alert("The person was born on " + month + " " + day + ", " + year + "."); 
    alert("The person is " + age + " years old.");

});

    function calcAge(year){

        var date = new Date();
        var currentYear = date.getFullYear();
        
        if(year == ""){throw Error("Cannont be bank, please enter a value")};
        if(isNaN(year)){throw Error("ERROR")};
        
        year = parseInt(year);

        if(year <= 0 ){throw Error("The given year is below zero, please enter a number above zero")};
        if(year > currentYear){throw Error("Year is after current year. Enter year before " + currentYear)};
        
        var yearsOld = currentYear - year;
        return yearsOld;
    }

});

