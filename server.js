// Variables
var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var app = express(); 
var server = require('http').createServer(app); 




app.set("views", path.resolve(__dirname, "views")); 
app.set("view engine", "ejs"); 
app.use(express.static(__dirname + '/assets'));

var entries = [];
app.locals.entries = entries; 



app.use(logger("dev")); 
app.use(bodyParser.urlencoded({ extended: false }));





// GET REQUEST
app.get("/", function (request, response) {
    response.sendFile(__dirname+"/views/Moody_Christopher.html");
});
app.get("/home", function (request, response) {
    response.sendFile(__dirname+"/views/Moody_Christopher.html");
});
app.get("/application", function (request, response) {
    response.sendFile(__dirname+"/views/Application.html");
});
app.get("/contact", function (request, response) {
    response.sendFile(__dirname+"/views/Contact.html");
});
app.get("/new-entry", function (request, response) {
    response.render("new-entry");
});
app.get("/guestbook", function (request, response) {
    response.render("index");
});






// POSTS REQS
app.post("/new-entry", function (request, response) {
        if (!request.body.title || !request.body.body) { response.status(400).send("Entries must have a title and a body.");return;}
        entries.push({ // store it
        title: request.body.title,
        content: request.body.body,
        published: new Date()
    });
    response.redirect("/guestbook"); 
    });
// 404
app.use(function (request, response) {
    response.status(404).render("404");
    });
// Listen for an application request on port 8081
server.listen(8081, function () {
 console.log('Guestbook app listening on http://127.0.0.1:8081/');
});