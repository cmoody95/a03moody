# A03 Personal Website

A simple personal website for Web Apps & Services - NWMSU 2017

## How to use

Open a command window in your project folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8081`. 